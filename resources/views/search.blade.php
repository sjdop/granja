@extends('layout')

@section('content')
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Buscar movimiento</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <form id="main-form" method="post" action="{{ route('vegetable.store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group{{ $errors->has('id') ? ' has-error' : ''}}">
                            <label for="subject">Hortaliza</label>
                            <input id="id-txt" type="text" class="form-control" name="id">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group{{ $errors->has('harvest_time') ? ' has-error' : ''}}">
                            <label for="subject">Actividad</label>
                            <input id="activity-txt" type="text" class="form-control" name="activity_id">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
                            <label for="type">Empleado</label>
                            <input id="employee-txt" type="text" class="form-control" name="employee_id">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label><input type="radio" name="criteria" value="like"> Contiene...</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label><input type="radio" name="criteria" value="starts"> Empiece por...</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label><input type="radio" name="criteria" value="ends"> Termine en...</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label><input type="radio" name="criteria" value="equals"> Igual a...</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <a id="remove-btn" href="" class="btn btn-warning">Quitar filtro</a>
                        <a id="accept-btn" href="" class="btn btn-success">Aceptar</a>
                        <a id="cancel-btn" href="" class="btn btn-danger">Cancelar</a>
                    </div>
                    <div class="col-sm-3">
                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Actividad</th>
                        <th>Hortaliza</th>
                        <th>Duración actividad</th>
                        <th>Empleado</th>
                        <th>Fecha actividad</th>
                    </tr>
                </thead>
                <tbody id="table-body">

                </tbody>
            </table>
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection

@section('javascript')
    <script type="text/javascript">
        var url = "{{ route('search') }}";

        $('#accept-btn').on('click', function() {
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    vegetable: $('#id-txt').val(),
                    activity: $('#activity-txt').val(),
                    employee: $('#employee-txt').val(),
                    criteria: $('input[name="criteria"]:checked').val()
                },
                success: function(res) {
                    var elements = ['id','activity','vegetable','activity_duration','employee','activity_date'];
                    $('#table-body').html('');
                    for (var i = 0; i < res.length; i++) {
                        var row = $('<tr>');
                        for (e in elements) {
                            var element = $('<td>');
                            $(element).html(res[i][elements[e]]);
                            $(row).append(element);
                        }
                        $('#table-body').append(row);
                    }
                },
                error: function(err) {
                    alert("Ha ocurrido un error, intenta nuevamente");
                }
            })
            return false;
        });
        $('#remove-btn').on('click', function() {

            $('#id-txt').val(''),
            $('#activity-txt').val(''),
            $('#employee-txt').val(''),
            $('input[name="criteria"]:checked').prop('checked', false);
            return false;
        });
    </script>
@endsection