@extends('layout')

@section('content')
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Crear hortaliza</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <button id="new-btn" class="btn btn-info">Nuevo</button>
            <hr/>
            <form id="main-form" method="post" action="{{ route('vegetable.store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('id') ? ' has-error' : ''}}">
                            <label for="subject">Id Hortaliza</label>
                            <input id="id-txt" type="text" class="form-control" name="name" disabled="disabled">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('harvest_time') ? ' has-error' : ''}}">
                            <label for="subject">Tiempo cosecha</label>
                            <input id="harvest-time-txt" type="text" class="form-control" name="harvest_time">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
                            <label for="type">Descripción</label>
                            <input id="description-txt" type="text" class="form-control" name="description">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
                            <label for="type">Estado</label>
                            <input id="state-txt" type="text" class="form-control" name="state">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('seedtime') ? ' has-error' : ''}}">
                            <label for="type">Tipo siembra</label>
                            <input id="seedtime-txt" type="text" class="form-control" name="seedtime">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('distancing') ? ' has-error' : ''}}">
                            <label for="type">Distanciamiento</label>
                            <input id="distancing-txt" type="text" class="form-control" name="distancing">
                        </div>
                    </div>
                </div>
                <button id="submit" class="btn btn-info btn-block">Enviar</button>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Tiempo cosecha</th>
                    <th>Descripción</th>
                    <th>Estado</th>
                    <th>Tipo siembra</th>
                    <th>Distanciamiento</th>
                    <th>Acciones</th>
                </tr>
                @foreach($vegetables as $vegetable)
                    <tr>
                        <td>{{ $vegetable->id }}</td>
                        <td>{{ $vegetable->harvest_time }}</td>
                        <td>{{ $vegetable->description }}</td>
                        <td>{{ $vegetable->state }}</td>
                        <td>{{ $vegetable->seedtime }}</td>
                        <td>{{ $vegetable->distancing }}</td>
                        <td>
                            <a href="" class="btn btn-warning edit" data-toggle="modal" data-target="#answer-modal" data-id="{{ $vegetable->id }}"><i class="fa fa-sticky-note"></i></a>
                            <a href="" data-id="{{ $vegetable->id }}" class="btn btn-danger delete"><i class="fa fa-ban"></i></a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3">
                        {{ $vegetables->links() }}
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection

@section('javascript')
    <script type="text/javascript">
        var url = "{{ route('vegetable.store') }}";

        $('.delete').on('click', function(evt) {
            if (confirm("Seguro que desea eliminar este tutorial?") ) {
                var id = $(this).data('id');
                var target = this;
                $.ajax({
                    url: url + "/" + id,
                    method: "POST",
                    data: {
                        "_method": "DELETE"
                    },
                    success: function(res) {
                        $(target).parents('tr').fadeOut();
                    },
                    error: function(err) {
                        alert("Ha ocurrido un error, intenta nuevamente");
                    }
                });
            }
            return false;
        });

        $('.edit').on('click', function(evt) {
            var row = $(this).parent().parent().find('td');
            var id = $(row[0]).html();
            $('#id-txt').val(id);
            $('#harvest-time-txt').val($(row[1]).html());
            $('#description-txt').val($(row[2]).html());
            $('#state-txt').val($(row[3]).html());
            $('#seedtime-txt').val($(row[4]).html());
            $('#distancing-txt').val($(row[5]).html());
            $('#main-form').attr('action', url + "/" + id);
            var method = $('<input>');
            $(method).attr('id', 'method-input');
            $(method).attr('type', 'hidden');
            $(method).attr('name', '_method');
            $(method).val('put');
            $('#main-form').append(method);
        });

        $('#new-btn').on('click', function() {
            $('#method-input').remove();
            $('#main-form').attr('action', url);
            $('#id-txt').val('');
            $('#harvest-time-txt').val('');
            $('#description-txt').val('');
            $('#state-txt').val('');
            $('#seedtime-txt').val('');
            $('#distancing-txt').val('');
        });
    </script>
@endsection