@extends('layout')

@section('content')
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Crear empleado</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <button id="new-btn" class="btn btn-info">Nuevo</button>
            <hr/>
            <form id="main-form" method="post" action="{{ route('employee.store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('id') ? ' has-error' : ''}}">
                            <label for="subject">Id Empleado</label>
                            <input id="id-txt" type="text" class="form-control" name="id" disabled="disabled">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
                            <label for="subject">Teléfono</label>
                            <input id="phone-txt" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                            <label for="type">Nombre</label>
                            <input id="name-txt" type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : ''}}">
                            <label for="type">Categoría</label>
                            <select id="category-cmb" name="category_id" class="form-control">
                            @foreach($categories as $k => $v)
                                <option value="{{ $k }}" @if($k == old('category_id')) selected @endif>{{ $v }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
                            <label for="type">Dirección</label>
                            <input id="address-txt" type="text" class="form-control" name="address" value="{{ old('address') }}">
                        </div>
                    </div>
                </div>
                <button id="submit" class="btn btn-info btn-block">Enviar</button>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Teléfono</th>
                    <th>Categoría</th>
                    <th>Dirección</th>
                    <th>Acciones</th>
                </tr>
                @foreach($employees as $employee)
                    <tr>
                        <td>{{ $employee->id }}</td>
                        <td>{{ $employee->name }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td data-id="{{ $employee->category->id }}">{{ $employee->category->description }}</td>
                        <td>{{ $employee->address }}</td>
                        <td>
                            <a href="" class="btn btn-warning edit" data-toggle="modal" data-target="#answer-modal" data-id="{{ $employee->id }}"><i class="fa fa-sticky-note"></i></a>
                            <a href="" data-id="{{ $employee->id }}" class="btn btn-danger delete"><i class="fa fa-ban"></i></a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3">
                        {{ $employees->links() }}
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection

@section('javascript')
    <script type="text/javascript">
        var url = "{{ route('employee.store') }}";

        $('.delete').on('click', function(evt) {
            if (confirm("Seguro que desea eliminar este tutorial?") ) {
                var id = $(this).data('id');
                var target = this;
                $.ajax({
                    url: url + "/" + id,
                    method: "POST",
                    data: {
                        "_method": "DELETE"
                    },
                    success: function(res) {
                        $(target).parents('tr').fadeOut();
                    },
                    error: function(err) {
                        alert("Ha ocurrido un error, intenta nuevamente");
                    }
                });
            }
            return false;
        });

        $('.edit').on('click', function(evt) {
            var row = $(this).parent().parent().find('td');
            var id = $(row[0]).html();
            $('#id-txt').val(id);
            $('#name-txt').val($(row[1]).html());
            $('#phone-txt').val($(row[2]).html());
            $('#category-cmb').val($(row[3]).data('id'));
            $('#address-txt').val($(row[4]).html());
            $('#main-form').attr('action', url + "/" + id);
            var method = $('<input>');
            $(method).attr('id', 'method-input');
            $(method).attr('type', 'hidden');
            $(method).attr('name', '_method');
            $(method).val('put');
            $('#main-form').append(method);
        });

        $('#new-btn').on('click', function() {
            $('#method-input').remove();
            $('#main-form').attr('action', url);
            $('#id-txt').val('');
            $('#name-txt').val('');
            $('#phone-txt').val('');
            $('#category-cmb').val('');
            $('#address-txt').val('');
        });
    </script>
@endsection