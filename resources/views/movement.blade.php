@extends('layout')

@section('content')
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Crear movimiento</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <button id="new-btn" class="btn btn-info">Nuevo</button>
            <hr/>
            <form id="main-form" method="post" action="{{ route('movement.store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('id') ? ' has-error' : ''}}">
                            <label for="subject">Id Movimiento</label>
                            <input id="id-txt" type="text" class="form-control" name="id" disabled="disabled">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('activity_id') ? ' has-error' : ''}}">
                            <label for="subject">Actividad</label>
                            <select id="activity-cmb" name="activity_id" class="form-control">
                                @foreach($activities as $k => $v)
                                    <option value="{{ $k }}" @if($k == old('activity_id')) selected @endif>{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('vegetable_id') ? ' has-error' : ''}}">
                            <label for="type">Hortaliza</label>
                            <select id="vegetable-cmb" name="vegetable_id" class="form-control">
                                @foreach($vegetables as $k => $v)
                                    <option value="{{ $k }}" @if($k == old('vegetable_id')) selected @endif>{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('activity_duration') ? ' has-error' : ''}}">
                            <label for="type">Duración actividad</label>
                            <input id="activity_duration-txt" type="text" class="form-control" name="activity_duration" value="{{ old('activity_duration') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : ''}}">
                            <label for="type">Empleado</label>
                            <select id="employee-cmb" name="employee_id" class="form-control">
                                @foreach($employees as $k => $v)
                                    <option value="{{ $k }}" @if($k == old('employee_id')) selected @endif>{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('activity_date') ? ' has-error' : ''}}">
                            <label for="type">Fecha actividad</label>
                            <input id="activity-date-txt" type="text" class="form-control" name="activity_date" value="{{ old('activity_date') }}">
                        </div>
                    </div>
                </div>
                <button id="submit" class="btn btn-info btn-block">Enviar</button>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Actividad</th>
                    <th>Hortaliza</th>
                    <th>Duración actividad</th>
                    <th>Empleado</th>
                    <th>Fecha actividad</th>
                    <th>Acciones</th>
                </tr>
                @foreach($movements as $movement)
                    <tr>
                        <td>{{ $movement->id }}</td>
                        <td data-id="{{ $movement->activity->id }}">{{ $movement->activity->description }}</td>
                        <td data-id="{{ $movement->vegetable->id }}">{{ $movement->vegetable->description }}</td>
                        <td>{{ $movement->activity_duration }}</td>
                        <td data-id="{{ $movement->employee->id }}">{{ $movement->employee->name }}</td>
                        <td>{{ $movement->activity_date->format('Y-m-d') }}</td>
                        <td>
                            <a href="" class="btn btn-warning edit" data-toggle="modal" data-target="#answer-modal" data-id="{{ $movement->id }}"><i class="fa fa-sticky-note"></i></a>
                            <a href="" data-id="{{ $movement->id }}" class="btn btn-danger delete"><i class="fa fa-ban"></i></a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3">
                        {{ $movements->links() }}
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection

@section('javascript')
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        //Date picker
        $('#activity-date-txt').datepicker({
            format: 'yyyy-mm-dd',
            language: 'es',
            autoclose: true
        })
        var url = "{{ route('movement.store') }}";

        $('.delete').on('click', function(evt) {
            if (confirm("Seguro que desea eliminar este tutorial?") ) {
                var id = $(this).data('id');
                var target = this;
                $.ajax({
                    url: url + "/" + id,
                    method: "POST",
                    data: {
                        "_method": "DELETE"
                    },
                    success: function(res) {
                        $(target).parents('tr').fadeOut();
                    },
                    error: function(err) {
                        alert("Ha ocurrido un error, intenta nuevamente");
                    }
                });
            }
            return false;
        });

        $('.edit').on('click', function(evt) {
            var row = $(this).parent().parent().find('td');
            var id = $(row[0]).html();
            $('#id-txt').val(id);
            $('#activity-cmb').val($(row[1]).data('id'));
            $('#vegetable-cmb').val($(row[2]).data('id'));
            $('#activity_duration-txt').val($(row[3]).html());
            $('#employee-cmb').val($(row[4]).data('id'));
            $('#activity-date-txt').val($(row[5]).html());
            $('#main-form').attr('action', url + "/" + id);
            var method = $('<input>');
            $(method).attr('id', 'method-input');
            $(method).attr('type', 'hidden');
            $(method).attr('name', '_method');
            $(method).val('put');
            $('#main-form').append(method);
        });

        $('#new-btn').on('click', function() {
            $('#method-input').remove();
            $('#main-form').attr('action', url);
            $('#id-txt').val('');
            $('#activity-cmb').val('');
            $('#vegetable-cmb').val('');
            $('#activity_duration-txt').val('');
            $('#employee-cmb').val('');
            $('#activity-date-txt').val('');
        });
    </script>
@endsection