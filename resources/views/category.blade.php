@extends('layout')

@section('content')
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Crear categoría</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <button id="new-btn" class="btn btn-info">Nuevo</button>
            <hr/>
            <form id="main-form" method="post" action="{{ route('category.store') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('id') ? ' has-error' : ''}}">
                    <label for="subject">Id Categoría</label>
                    <input id="id-txt" type="text" class="form-control" name="name" disabled="disabled">
                </div>
                <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
                    <label for="type">Descripción</label>
                    <input id="description-txt" type="text" class="form-control" name="description" value="{{ old('description') }}">
                </div>
                <div class="form-group{{ $errors->has('hour_value') ? ' has-error' : ''}}">
                    <label for="type">Valor hora</label>
                    <input id="hour-value-txt" type="text" class="form-control" name="hour_value" value="{{ old('hour_value') }}">
                </div>
                <button id="submit" class="btn btn-info btn-block">Enviar</button>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Descripción</th>
                    <th>Acciones</th>
                </tr>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->description }}</td>
                        <td>{{ $category->hour_value }}</td>
                        <td>
                            <a href="" class="btn btn-warning edit" data-toggle="modal" data-target="#answer-modal" data-id="{{ $category->id }}"><i class="fa fa-sticky-note"></i></a>
                            <a href="" data-id="{{ $category->id }}" class="btn btn-danger delete"><i class="fa fa-ban"></i></a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3">
                        {{ $categories->links() }}
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection

@section('javascript')
    <script type="text/javascript">
        var url = "{{ route('category.store') }}";

        $('.delete').on('click', function(evt) {
            if (confirm("Seguro que desea eliminar este tutorial?") ) {
                var id = $(this).data('id');
                var target = this;
                $.ajax({
                    url: url + "/" + id,
                    method: "POST",
                    data: {
                        "_method": "DELETE"
                    },
                    success: function(res) {
                        $(target).parents('tr').fadeOut();
                    },
                    error: function(err) {
                        alert("Ha ocurrido un error, intenta nuevamente");
                    }
                });
            }
            return false;
        });

        $('.edit').on('click', function(evt) {
            var row = $(this).parent().parent().find('td');
            var id = $(row[0]).html();
            $('#id-txt').val(id);
            $('#description-txt').val($(row[1]).html());
            $('#hour-value-txt').val($(row[2]).html());
            $('#main-form').attr('action', url + "/" + id);
            var method = $('<input>');
            $(method).attr('id', 'method-input');
            $(method).attr('type', 'hidden');
            $(method).attr('name', '_method');
            $(method).val('put');
            $('#main-form').append(method);
        });

        $('#new-btn').on('click', function() {
            $('#method-input').remove();
            $('#main-form').attr('action', url);
            $('#id-txt').val("");
            $('#description-txt').val("");
            $('#hour-value-txt').val("");
        });
    </script>
@endsection