<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    private $rules = [
        'phone' => 'required',
        'name' => 'required',
        'category' => 'required|integer',
        'address' => 'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Empleados";
        $employees = Employee::paginate(10);
        $categories = Category::get()->pluck('description', 'id');
        return view('employee', compact('title', 'employees', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        Employee::create($request->all());
        return redirect()->route('employee.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $this->validate($request, $this->rules);
        \Session::flash('success', $employee->update($request->all()));
        return redirect()->route('activity.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        if ( $employee->delete() ) {
            return response()->json(array("success" => true));
        }
        return response()->json(array("success" => false), 404);
    }
}
