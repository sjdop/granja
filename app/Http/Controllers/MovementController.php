<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Employee;
use App\Models\Movement;
use App\Models\Vegetable;
use Illuminate\Http\Request;

class MovementController extends Controller
{

    private $rules = [
        'activity_id' => 'required|integer',
        'vegetable_id' => 'required|integer',
        'activity_duration' => 'required',
        'employee_id' => 'required|integer',
        'activity_date' => 'required|date'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Movimientos";
        $movements = Movement::paginate(10);
        $activities = Activity::get()->pluck('description', 'id');
        $vegetables = Vegetable::get()->pluck('description', 'id');
        $employees = Employee::get()->pluck('name', 'id');
        return view('movement', compact('title', 'movements', 'activities', 'vegetables', 'employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        Movement::create($request->all());
        return redirect()->route('movement.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movement  $movement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movement $movement)
    {
        $this->validate($request, $this->rules);
        \Session::flash('success', $movement->update($request->all()));
        return redirect()->route('activity.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movement  $movement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movement $movement)
    {
        if ( $movement->delete() ) {
            return response()->json(array("success" => true));
        }
        return response()->json(array("success" => false), 404);
    }

    /**
     * Search a movement by criteria
     */
    public function getSearch(Request $request) {
        $title = "Buscar";
        return view('search', compact('title'));
    }

    public function postSearch(Request $request) {
        $query = \DB::table('movements')
            ->join('activities', 'movements.activity_id', '=', 'activities.id')
            ->join('employees', 'movements.employee_id', '=', 'employees.id')
            ->join('vegetables', 'movements.vegetable_id', '=', 'vegetables.id')
            ->select('movements.*', 'employees.name AS employee',
                'activities.description AS activity', 'vegetables.description AS vegetable');
        $object = $request->all();
        $criteria = array_key_exists('criteria', $object) ? $object['criteria'] : '';
        switch($criteria) {
            case 'like':
                $query->where('activities.description', 'like', "%" . $object['activity'] . "%")
                    ->orWhere('employees.name', 'like', "%" . $object['employee'] . "%")
                    ->orWhere('vegetables.description', 'like', "%" . $object['vegetable'] . "%");
                break;
            case 'starts':
                $query->where('activities.description', 'like', $object['activity'] . "%")
                    ->orWhere('employees.name', 'like', $object['employee'] . "%")
                    ->orWhere('vegetables.description', 'like', $object['vegetable'] . "%");
                break;
            case 'ends':
                $query->where('activities.description', 'like', "%" . $object['activity'])
                    ->orWhere('employees.name', 'like', "%" . $object['employee'])
                    ->orWhere('vegetables.description', 'like', "%" . $object['vegetable']);
                break;
            case 'equals':
                $query->where('activities.description', '=', $object['activity'])
                    ->orWhere('employees.name', '=', $object['employee'])
                    ->orWhere('vegetables.description', '=', $object['vegetable']);
                break;
        }
        return $query->get();
    }
}
