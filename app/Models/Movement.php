<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{

    protected $fillable = [
        'activity_id', 'vegetable_id', 'activity_duration',
        'employee_id', 'activity_date'
    ];

    protected $dates = ['activity_date'];

    public function activity() {
        return $this->belongsTo('App\Models\Activity');
    }

    public function vegetable() {
        return $this->belongsTo('App\Models\Vegetable');
    }

    public function employee() {
        return $this->belongsTo('App\Models\Employee');
    }

}
