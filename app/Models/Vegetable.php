<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vegetable extends Model
{

    protected $fillable = [
        'harvest_time', 'description', 'state',
        'seedtime', 'distancing'];

}
