<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('movement.index');
});

Route::resource('activity', 'ActivityController')->except(['create', 'show', 'edit']);
Route::resource('category', 'CategoryController')->except(['create', 'show', 'edit']);
Route::resource('vegetable', 'VegetableController')->except(['create', 'show', 'edit']);
Route::resource('employee', 'EmployeeController')->except(['create', 'show', 'edit']);
Route::resource('movement', 'MovementController')->except(['create', 'show', 'edit']);

Route::get('search', 'MovementController@getSearch')->name('search.index');
Route::post('search', 'MovementController@postSearch')->name('search');